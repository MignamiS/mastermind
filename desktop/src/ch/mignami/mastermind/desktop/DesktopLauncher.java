package ch.mignami.mastermind.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import ch.mignami.mastermind.MMGame;
import ch.mignami.mastermind.MMGameCommandLine;

public class DesktopLauncher {

	private static void commandLineMain() {
		System.out.println("start");
		MMGameCommandLine.init();
		boolean finished = false;
		do {
			finished = !MMGameCommandLine.round();
			finished = finished || MMGameCommandLine.checkVictory();
		} while (!finished);
		System.out.println("Finish");
		if (MMGameCommandLine.checkVictory())
			System.out.println("Victory!");
		else
			System.out.println("You loose...");
		MMGameCommandLine.dispose();
	}

	private static void graphicalMain() {
		final LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 300;
		config.height = 480;
		config.resizable = false;
		new LwjglApplication(new MMGame(), config);
	}

	public static void main(final String[] arg) {
		if (arg.length > 0 && arg[0].startsWith("-c"))
			commandLineMain();
		else
			graphicalMain();
	}
}
