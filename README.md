Master Mind game
-----------------

Author
:	Simone Mignami <simone.mig29@gmail.com>

Description
:	The game of [Master Mind](https://en.wikipedia.org/wiki/Mastermind_(board_game)) created using Java and [LibGDX](https://libgdx.badlogicgames.com/).
