package ch.mignami.mastermind.game;

import static org.junit.Assert.*;

import org.junit.Test;

public class AttemptTest {

	@Test
	public void testBuild() {
		final int[] att = new int[] { 1, 2, 3, 4 };
		final Attempt attempt = Attempt.build(att);
		for (int i = 0; i < att.length; i++)
			assertEquals(att[i], attempt.getAttempt()[i].getValue());
	}

}
