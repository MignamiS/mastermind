package ch.mignami.mastermind.game;

import org.junit.Test;

import static org.junit.Assert.*;

public class MatchTest {

	@Test
	public void testBuild() {
		// check just does not throws index-out-of-bound exception
		for (int i = 0; i < 100; i++)
			Match.build();
	}

	@Test
	public void testAvailableAttempt() {
		final Match m = Match.build();

		// all
		assertEquals(Match.ATTEMPT_NUMBER, m.availableAttempt());
		// all - 1
		final Attempt att = Attempt.build(new int[] { 0, 0, 0, 0 });
		m.addAttempt(att);
		assertEquals(Match.ATTEMPT_NUMBER - 1, m.availableAttempt());
	}

	@Test
	public void testCanProceed() {
		final Match m = Match.build();
		final int[] att = new int[] { 1, 2, 3, 4 };
		for (int i = 1; i <= Match.ATTEMPT_NUMBER + 1; i++) {
			final Attempt a = Attempt.build(att);
			m.addAttempt(a);
			// System.out.println("" + i + " " + m.canProceed());
			if (i <= Match.ATTEMPT_NUMBER)
				assertTrue(m.canProceed());
			else
				assertFalse(m.canProceed());

		}
	}

	@Test
	public void testCheckAttempt() {
		final Match m = Match.build(new int[] { 3, 3, 3, 1 });

		// 0 place, 1 digit
		final Attempt a = Attempt.build(new int[] { 1, 0, 4, 5 });
		m.checkAttempt(a);
		assertEquals(0, a.getRightPlace());
		assertEquals(1, a.getRightDigit());
		// 1 place, 1 digit
		final Attempt a2 = Attempt.build(new int[] { 3, 0, 4, 5 });
		m.checkAttempt(a2);
		assertEquals(1, a2.getRightPlace());
		assertEquals(1, a2.getRightDigit());
		// 0 place, 2 digit
		final Attempt a3 = Attempt.build(new int[] { 1, 0, 4, 3 });
		m.checkAttempt(a3);
		assertEquals(0, a3.getRightPlace());
		assertEquals(2, a3.getRightDigit());
		// 2 place, 2 digit
		final Attempt a4 = Attempt.build(new int[] { 3, 0, 4, 1 });
		m.checkAttempt(a4);
		assertEquals(2, a4.getRightPlace());
		assertEquals(2, a4.getRightDigit());
		// win
		final Attempt b = Attempt.build(new int[] { 3, 3, 3, 1 });
		m.checkAttempt(b);
		assertEquals(4, b.getRightPlace());
		assertEquals(4, b.getRightDigit());
	}

}
