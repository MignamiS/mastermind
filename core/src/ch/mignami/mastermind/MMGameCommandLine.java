package ch.mignami.mastermind;

import java.util.Arrays;
import java.util.Scanner;

import ch.mignami.mastermind.game.Attempt;
import ch.mignami.mastermind.game.Digit;
import ch.mignami.mastermind.game.Match;

/**
 * This class contains the code for playing Master Mind using the command line.
 * This is particularly suitable for blind and visual impaired people.
 *
 * @author Simone Mignami
 *
 */
public class MMGameCommandLine {

	private static Match currentMatch;
	private static Scanner scanner;

	/**
	 * Checks if the user won.
	 *
	 * @return <code>true</code> if the user won, <code>false</code> otherwise.
	 *         Calling this method when the match is not finished will return
	 *         <code>false</code>
	 */
	public static boolean checkVictory() {
		final Attempt last = currentMatch.getLastAttempt();
		if (last != null)
			return last.getRightPlace() == 4;
		return false;
	}

	public static void dispose() {
		if (scanner != null) {
			scanner.close();
			scanner = null;
		}
	}

	private static int[] getAttempt() {
		final int[] fetch = new int[4];
		for (int i = 0; i < 4; i++) {
			final int temp = sanitize();
			fetch[i] = temp;
		}
		scanner.nextLine();

		return fetch;
	}

	/**
	 * Initialize the game with a new match.
	 */
	public static void init() {
		currentMatch = Match.build();
		if (scanner == null)
			scanner = new Scanner(System.in);
		// printSolution();
	}

	private static void printResult(final Attempt attempt) {
		final String str = String.format(
				"Your attempt: %s%nDigit: %d\tPlace: %d", attempt.toString(),
				attempt.getRightDigit(), attempt.getRightPlace());
		System.out.println(str);
	}

	public static void printSolution() {
		final Digit[] solution = currentMatch.getSolution();
		System.out.println(Arrays.toString(solution));
	}

	private static void printStats() {
		final String str = String.format("Round %d of %d",
				currentMatch.getAttemptNumber(), Match.ATTEMPT_NUMBER);
		System.out.println(str);
	}

	/**
	 * A single round, composed by a player attempt, a check and a response.
	 *
	 * @return whether another round is possible. If <code>false</code> it can
	 *         be possible that there are no more attempt available or the user
	 *         win
	 */
	public static boolean round() {
		if (currentMatch == null)
			return false;

		printStats();
		final int[] fetch = getAttempt();
		final Attempt attempt = Attempt.build(fetch);
		currentMatch.checkAttempt(attempt);
		printResult(attempt);
		currentMatch.addAttempt(attempt);
		return currentMatch.canProceed();
	}

	private static int sanitize() {
		if (!scanner.hasNextInt())
			return 0;

		final int temp = scanner.nextInt();
		if (temp < Digit.minRange())
			return 0;
		else if (temp > Digit.maxRange())
			return Digit.maxRange();

		return temp;
	}
}
