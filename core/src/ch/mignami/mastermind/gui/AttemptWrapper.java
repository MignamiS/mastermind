package ch.mignami.mastermind.gui;

import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

import ch.mignami.mastermind.game.Attempt;

public class AttemptWrapper {

	private Attempt attempt;
	private final WidgetGroup ui;

	public AttemptWrapper(final Skin skin) {
		this.ui = buildUI(skin);
	}

	private WidgetGroup buildUI(final Skin skin) {
		// pogs
		final HorizontalGroup pogGroup = new HorizontalGroup();
		final HorizontalGroup resultGroup = new HorizontalGroup();
		for (int i = 0; i < 4; i++) {
			pogGroup.addActor(new TextButton("b" + i, skin));
			resultGroup.addActor(new TextButton("R" + i, skin));
		}
		// container
		final HorizontalGroup group = new HorizontalGroup();
		group.addActor(pogGroup);
		group.addActor(resultGroup);
		return group;
	}

	/**
	 * Transform the UI color combination into an "internal real" attempt.
	 */
	public void submitAttempt() {
		// build attempt
		// submit to match
		// add pogs for score
		// freeze
	}

	public WidgetGroup getUi() {
		return ui;
	}
}
