package ch.mignami.mastermind.gui;

import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

import ch.mignami.mastermind.game.Match;

public class MatchContainer {

	private Match match;
	private final WidgetGroup root;

	public MatchContainer(final int[] solution, final Skin skin) {
		if (solution == null)
			this.match = match.build();
		else
			this.match = Match.build(solution);
		this.root = buildRoot(skin);
	}

	private WidgetGroup buildRoot(final Skin skin) {
		final WidgetGroup ui = new AttemptWrapper(skin).getUi();

		final ScrollPane scroller = new ScrollPane(ui);

		final Table root = new Table(skin);
		root.setFillParent(true);
		root.setDebug(true, true);
		root.add(scroller);
		return root;
	}

	public WidgetGroup getRoot() {
		return root;
	}
}
