package ch.mignami.mastermind;

import ch.mignami.mastermind.gui.MatchContainer;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class MMGame extends ApplicationAdapter {

	private Stage s;

	@Override
	public void create() {
		s = new Stage();
		final Skin skin = new Skin(
				Gdx.files.internal("skin/default/uiskin.json"));
		final MatchContainer mc = new MatchContainer(null, skin);
		s.addActor(mc.getRoot());
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		s.act(Gdx.graphics.getDeltaTime());
		s.draw();
	}
}
