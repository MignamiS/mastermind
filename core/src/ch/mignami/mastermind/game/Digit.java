package ch.mignami.mastermind.game;

/**
 * Represent a single digit of the secret code.
 *
 * @author Simone Mignami
 *
 */
public enum Digit {

	EIGHT(8), FIVE(5), FOUR(4), NINE(9), ONE(1), SEVEN(7), SIX(6), THREE(3), TWO(
			2), ZERO(0);

	// the real numeric value, used as label
	private final int value;

	private Digit(final int value) {
		this.value = value;
	}

	/**
	 * Like valueOf(), but returns <code>null</code> instead throwing an
	 * exception.
	 *
	 * @param val
	 *            the int value
	 * @return the corresponding digit, or null
	 */
	public static Digit fetchFromInteger(final int val) {
		for (final Digit digit : values()) {
			if (digit.getValue() == val)
				return digit;
		}
		return null;
	}

	public int getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "" + this.getValue();
	}

	public static int minRange() {
		return 0;
	}

	public static int maxRange() {
		return 9;
	}
}
