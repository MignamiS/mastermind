package ch.mignami.mastermind.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * A game match, with a solution and a sequence of attempts.
 *
 * @author Simone Mignami
 *
 */
public class Match {

	public static final int ATTEMPT_NUMBER = 9;
	public static final int CODE_SIZE = 4;

	private final List<Attempt> attempts;
	private final Digit[] solution;

	private Match(final Digit[] solution) {
		this.solution = solution;
		this.attempts = new ArrayList<Attempt>(ATTEMPT_NUMBER);
	}

	/**
	 * Builds a new match with a random solution.
	 *
	 * @return a new match
	 */
	public static Match build() {
		final Random r = new Random();
		final int totalDigits = Digit.values().length;
		final Digit[] solution = new Digit[CODE_SIZE];
		for (int i = 0; i < solution.length; i++) {
			final int index = r.nextInt(totalDigits);
			solution[i] = Digit.values()[index];
		}

		return new Match(solution);
	}

	/**
	 * Builds a new match with the given solution (for test purpose).
	 *
	 * @param solution
	 *            the given solution
	 * @return the new match
	 */
	public static Match build(final int[] solution) {
		final Digit[] realSolution = new Digit[solution.length];
		for (int i = 0; i < realSolution.length; i++) {
			final Digit fetch = Digit.fetchFromInteger(solution[i]);
			if (fetch == null)
				throw new IllegalStateException("Unrecognized digit: "
						+ solution[i]);
			realSolution[i] = fetch;
		}

		return new Match(realSolution);

	}

	/**
	 * Adds an attempt to the attempts list. This method does not check if the
	 * max number of attempts has been exceeded.
	 *
	 * @param attempt
	 */
	public void addAttempt(final Attempt attempt) {
		this.attempts.add(attempt);
	}

	/**
	 * Returns the current available attempt.
	 *
	 * @return how many attempts allowed
	 */
	public int availableAttempt() {
		return ATTEMPT_NUMBER - this.attempts.size();
	}

	/**
	 * Checks if the total amount of attempts has been reached or not.
	 *
	 * @return whether the total amount of attempts has been reached
	 */
	public boolean canProceed() {
		return this.attempts.size() < ATTEMPT_NUMBER;
	}

	/**
	 * Compare the given attempt with the solution and sets its stats with the
	 * response (side effect). Calling this method more than one time has no
	 * effect.
	 *
	 * @param attempt
	 */
	public void checkAttempt(final Attempt attempt) {
		checkRightDigit(attempt);
		checkRightPlace(attempt);
	}

	private void checkRightDigit(final Attempt attempt) {
		final int digitNumber = Digit.values().length;
		// count times in solution
		final int[] solutionCounter = new int[digitNumber];
		Arrays.fill(solutionCounter, 0);
		for (final Digit digit : this.solution)
			solutionCounter[digit.getValue()]++;

		// count times in attempt
		final int[] attemptCounter = new int[digitNumber];
		Arrays.fill(attemptCounter, 0);
		for (final Digit digit : attempt.getAttempt())
			attemptCounter[digit.getValue()]++;

		// look in solution for non-zero counters
		int found = 0;
		for (int i = 0; i < solutionCounter.length; i++) {
			if (solutionCounter[i] > 0)
				found += Math.min(solutionCounter[i], attemptCounter[i]);
		}

		attempt.setRightDigit(found);
	}

	private void checkRightPlace(final Attempt attempt) {
		int found = 0;
		final Digit[] attValues = attempt.getAttempt();
		for (int i = 0; i < attValues.length; i++) {
			if (attValues[i].equals(this.solution[i]))
				found++;
		}
		attempt.setRightPlace(found);
	}

	/**
	 * Returns the number of attempts occurred on this match.
	 *
	 * @return int starting from 1
	 */
	public int getAttemptNumber() {
		return this.attempts.size() + 1;
	}

	/**
	 * Returns the last attempt of the user.
	 *
	 * @return the last attempt, or <code>null</code> if no one is found
	 */
	public Attempt getLastAttempt() {
		if (this.attempts.size() > 0)
			return this.attempts.get(attempts.size() - 1);
		else
			return null;
	}

	public Digit[] getSolution() {
		return solution;
	}

	@Override
	public String toString() {
		final String sol = Arrays.toString(this.solution);
		return String.format("Match %s with %d attempt(s)", sol,
				this.attempts.size());
	}
}
