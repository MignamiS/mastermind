package ch.mignami.mastermind.game;

import java.util.Arrays;

/**
 * Represent a single attempt to solve the secret code.
 *
 * @author Simone Mignami
 *
 */
public class Attempt {

	// the sequence of digits for this attempt
	private final Digit[] attempt;
	// Correct digit on the code, no place check
	private int rightDigit = 0;
	// correct number of digits at the correct place
	private int rightPlace = 0;

	public Attempt(final Digit[] attempt) {
		this.attempt = attempt;
	}

	/**
	 * Factory method that builds an attempt from the given sequence of
	 * integers.
	 *
	 * @param attempt
	 * @return an attempt
	 */
	public static Attempt build(final int[] attempt) {
		final Digit[] realAttempt = new Digit[attempt.length];
		for (int i = 0; i < realAttempt.length; i++) {
			final Digit fetch = Digit.fetchFromInteger(attempt[i]);
			if (fetch == null)
				throw new IllegalStateException("Unrecognized digit: "
						+ attempt[i]);
			realAttempt[i] = fetch;
		}

		return new Attempt(realAttempt);
	}

	/**
	 * Returns the digit' sequence
	 *
	 * @return the sequence
	 */
	public Digit[] getAttempt() {
		return attempt;
	}

	/**
	 * Returns the correct digit number of the attempt, without checking the
	 * place.
	 *
	 * @return correct digit number
	 */
	public int getRightDigit() {
		return rightDigit;
	}

	/**
	 * Returns the amount of correct digit at the correct place.
	 *
	 * @return correct digit at the right place
	 */
	public int getRightPlace() {
		return rightPlace;
	}

	public void setRightDigit(final int rightDigit) {
		this.rightDigit = (rightDigit < 0) ? 0 : rightDigit;
	}

	public void setRightPlace(final int rightPlace) {
		this.rightPlace = (rightPlace < 0) ? 0 : rightPlace;
	}

	@Override
	public String toString() {
		return Arrays.toString(this.attempt);
	}

}
